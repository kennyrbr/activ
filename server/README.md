# activ server

`nvm use v16.15.1` - (`node16` alias)
`nvm use v14.16.0` - (`node14` alias)
`npm init -y`


## Run with   
- `npm run watch` - in one terminal - watches and compiles to js
- `npm run dev` - nodemons the compiled js

## Typeorm Migrations

[migrations](./docs/migrations.md)

## GraphQL

[graphql](./docs/graphql.md)