import { DataSource } from 'typeorm';

const source = new DataSource({
  type: 'mysql',
  host: 'localhost',
  port: 3306,
  username: 'root',
  password: 'root',
  database: 'activ',
  entities: ['src/entities/*.ts'],
  migrationsTableName: '_migrations',
  migrations: ['src/migrations/*.ts']
  //   type: 'sqlite',
  //   database: './storage/nest.db',
  //   entities: ['src/**/*.entity.ts'],
  //   migrationsTableName: 'migrations',
  //   migrations: ['src/orm/migrations/*.ts']
});

export default source;
