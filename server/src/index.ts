import 'reflect-metadata';
import path from 'path';
import express from 'express';
import cors from 'cors';
import { DataSource } from 'typeorm';
import { ApolloServer } from 'apollo-server-express';
import { ApolloServerPluginLandingPageGraphQLPlayground } from 'apollo-server-core';
import { buildSchema } from 'type-graphql';
import { SERVER_PORT, WEB_PORT } from './constants';
import { HelloResolver } from './resolvers/hello';
import { ActivType } from './entities/ActivType';
import { ActivTypeResolver } from './resolvers/ActivType';
import { ActivTypeAttribute } from './entities/ActivTypeAttribute';
import { ActivTypeAttributeMap } from './entities/ActivTypeAttributeMap';
import { ActivTypeAttributeResolver } from './resolvers/ActivTypeAttribute';

export let AppDataSource: DataSource;

const main = async () => {
  // typeorm - connection to database
  AppDataSource = new DataSource({
    type: 'mysql',
    host: 'localhost',
    port: 3306,
    username: 'root',
    password: 'root',
    database: 'activ',
    logging: true,
    // synchronize: true,
    migrationsTableName: '_migrations',
    migrations: [path.join(__dirname, './migrations/*')],
    entities: [ActivType, ActivTypeAttribute, ActivTypeAttributeMap]
  });

  await AppDataSource.initialize();
  console.log('Data Source has been initialized!');

  console.log('rerun server...');

  //   console.log('run the migrations...');
  //   await AppDataSource.runMigrations();

  // express - web server
  const app = express();

  // applying cors to all routes
  app.use(
    cors({
      origin: `http://localhost:${WEB_PORT}`,
      credentials: true
    })
  );

  //   app.get('/', (_, res) => {
  //     res.send('Howdy!');
  //   });

  // apollo - graphql server
  // plugins property - localhost:4000/graphq -> the playground to be used
  const apolloServer = new ApolloServer({
    schema: await buildSchema({
      resolvers: [HelloResolver, ActivTypeResolver, ActivTypeAttributeResolver],
      validate: false
    }),
    context: ({ req, res }) => ({ req, res }),
    plugins: [ApolloServerPluginLandingPageGraphQLPlayground()]
  });
  await apolloServer.start();
  // N.B., we are setting cors to false below BECAUSE we have already applied cors to express (see above)
  apolloServer.applyMiddleware({
    app,
    cors: false
  });

  app.listen(SERVER_PORT, () => {
    console.log(`server started on localhost:${SERVER_PORT}`);
  });
};

main().catch((err) => {
  console.error(err);
});
