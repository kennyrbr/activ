export const __prod__ = process.env.NODE_ENV === 'production';
export const COOKIE_NAME = 'qid';
export const SERVER_PORT = process.env.SERVER_PORT || 4001;
export const WEB_PORT = process.env.WEB_PORT || 3000;
