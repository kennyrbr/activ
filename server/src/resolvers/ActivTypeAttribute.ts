import { AppDataSource } from '../index';
import {
  Arg,
  Field,
  Int,
  Mutation,
  ObjectType,
  Query,
  Resolver
} from 'type-graphql';
import { ActivTypeAttribute } from '../entities/ActivTypeAttribute';
import { FieldError } from './FieldError';

@ObjectType()
class ActivTypeAttributeResponse {
  @Field(() => [FieldError], { nullable: true })
  errors?: FieldError[];

  @Field(() => ActivTypeAttribute, { nullable: true })
  attribute?: ActivTypeAttribute;
}

@Resolver(ActivTypeAttribute)
export class ActivTypeAttributeResolver {
  @Query(() => [ActivTypeAttribute])
  async activTypeAttributes(): Promise<ActivTypeAttribute[]> {
    const attributes = await ActivTypeAttribute.find();
    console.log(attributes);
    return attributes;
  }

  @Query(() => ActivTypeAttribute, { nullable: true })
  async activTypeAttribute(
    @Arg('id', () => Int) id: number
  ): Promise<ActivTypeAttribute | null> {
    return await ActivTypeAttribute.findOneBy({ id });
  }

  // @Mutation(() => ActivTypeAttributeResponse, { nullable: true })
  @Mutation(() => ActivTypeAttributeResponse)
  async createActivTypeAttribute(
    @Arg('title') title: string,
    @Arg('text') text: string,
    @Arg('type') type: string
  ): Promise<ActivTypeAttributeResponse> {
    const existing = await ActivTypeAttribute.findOneBy({ title });

    if (
      !!existing ||
      title.length <= 3 ||
      text.length <= 3 ||
      type.length <= 3
    ) {
      const errors = [];

      if (!!existing) {
        errors.push({
          field: 'title',
          message: 'title already exists'
        });
      }
      if (title.length <= 3) {
        errors.push({
          field: 'title',
          message: 'title must be longer than 3 characters'
        });
      }
      if (text.length <= 3) {
        errors.push({
          field: 'text',
          message: 'text must be longer than 3 characters'
        });
      }
      if (type.length <= 3) {
        errors.push({
          field: 'type',
          message: 'type must be longer than 3 characters'
        });
      }
      return { errors };
    }

    const created_by = 1;
    const updated_by = 1;
    // TODO: catch save errors and provide meaningful feedback to the user
    const activTypeAttribute = await AppDataSource.getRepository(
      ActivTypeAttribute
    ).save({
      title,
      text,
      type,
      created_by,
      updated_by
    });

    return { attribute: activTypeAttribute };
  }

  @Mutation(() => ActivTypeAttribute, { nullable: true })
  async updateActivTypeAttribute(
    @Arg('id', () => Int) id: number,
    @Arg('title') title: string,
    @Arg('text') text: string,
    @Arg('type') type: string
  ): Promise<ActivTypeAttribute | null> {
    // TODO: error handling on non-unique title
    await ActivTypeAttribute.update({ id }, { title, text, type });
    return await ActivTypeAttribute.findOneBy({ id });
  }

  @Mutation(() => Boolean, { nullable: true })
  async deleteActivTypeAttribute(
    @Arg('id', () => Int) id: number
  ): Promise<boolean> {
    await ActivTypeAttribute.delete({ id });
    return true;
  }
}
