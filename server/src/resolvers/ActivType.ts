import { Like } from 'typeorm';
import { Arg, Field, Int, Mutation, ObjectType, Query, Resolver } from 'type-graphql';
import { AppDataSource } from '../index';
import { ActivType } from '../entities/ActivType';
import { FieldError } from './FieldError';

@ObjectType()
class ActivTypeResponse {
  @Field(() => [FieldError], { nullable: true })
  errors?: FieldError[];

  @Field(() => ActivType, { nullable: true })
  type?: ActivType;
}

@Resolver(ActivType)
export class ActivTypeResolver {
  @Query(() => [ActivType])
  async activTypes(@Arg('query', () => String, { nullable: true }) query: string): Promise<ActivType[]> {
    let types;
    if (!!query) {
      types = await AppDataSource.getRepository(ActivType).findBy({
        title: Like(`%${query}%`)
      });
    } else {
      types = await ActivType.find();
    }
    return types;
  }

  @Query(() => ActivType, { nullable: true })
  async activType(@Arg('id', () => Int) id: number): Promise<ActivType | null> {
    return await ActivType.findOneBy({ id });
  }

  // @Mutation(() => ActivTypeResponse, { nullable: true })
  @Mutation(() => ActivTypeResponse)
  async createActivType(@Arg('title') title: string, @Arg('text') text: string): Promise<ActivTypeResponse> {
    const existing = await ActivType.findOneBy({ title });

    if (!!existing || title.length <= 3 || text.length <= 3) {
      const errors = [];

      if (!!existing) {
        errors.push({
          field: 'title',
          message: 'title already exists'
        });
      }
      if (title.length <= 3) {
        errors.push({
          field: 'title',
          message: 'title must be longer than 3 characters'
        });
      }
      if (text.length <= 3) {
        errors.push({
          field: 'text',
          message: 'text must be longer than 3 characters'
        });
      }
      return { errors };
    }

    // TODO: implement register/login and create/update with authenticated user
    const created_by = 1;
    const updated_by = 1;
    // TODO: catch save errors and provide meaningful feedback to the user
    const type = await AppDataSource.getRepository(ActivType).save({
      title,
      text,
      created_by,
      updated_by
    });

    return { type };
  }

  @Mutation(() => ActivType, { nullable: true })
  async updateActivType(
    @Arg('id', () => Int) id: number,
    @Arg('title') title: string,
    @Arg('text') text: string
  ): Promise<ActivType | null> {
    // TODO: error handling on non-unique title
    await ActivType.update({ id }, { title, text });
    return await ActivType.findOneBy({ id });
  }

  @Mutation(() => Boolean, { nullable: true })
  async deleteActivType(@Arg('id', () => Int) id: number): Promise<boolean> {
    await ActivType.delete({ id });
    return true;
  }
}
