import { MigrationInterface, QueryRunner } from 'typeorm';

export class ActivTypeTable1657350782395 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    queryRunner.query(`
    CREATE TABLE  activ_type  (
      id  int(11) NOT NULL AUTO_INCREMENT,
      title  varchar(255) NOT NULL,
      text  varchar(255) NOT NULL,
      created_at  datetime(6) NOT NULL DEFAULT current_timestamp(6),
      created_by  int(11) NOT NULL,
      updated_at  datetime(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
      updated_by  int(11) NOT NULL,
      deleted_at  datetime(6) DEFAULT NULL,
      deleted_by  int(11) DEFAULT NULL,
     PRIMARY KEY ( id )
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
`);
  }

  public async down(): Promise<void> {}
}
