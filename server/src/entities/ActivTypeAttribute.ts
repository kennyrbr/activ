import { ObjectType, Field, Int } from 'type-graphql';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  BaseEntity,
  DeleteDateColumn
} from 'typeorm';

// e.g.,
//   - weight
//   - repetitions
//   - duration
//   - distance
//
// used like:
// bench press
//   - weight
//   - repetitions
// pushups =>
//   - repetitions
// walk
//   - duration time (milliseconds)
//   - distance (int)
//
// input
//   - html form element
// output
//   - display (text)
//   - graphical display
@ObjectType()
@Entity()
export class ActivTypeAttribute extends BaseEntity {
  @Field(() => Int)
  @PrimaryGeneratedColumn()
  id!: number;

  @Field(() => String)
  @Column({ unique: true })
  title!: string;

  @Field()
  @Column()
  text!: string;

  // type
  // - the 'react' element type?
  // - the 'html' input type?
  @Field()
  @Column()
  type!: string;

  @Field()
  @CreateDateColumn()
  created_at: Date;

  @Field()
  @Column()
  created_by: number;

  @Field()
  @UpdateDateColumn()
  updated_at: Date;

  @Field()
  @Column()
  updated_by: number;

  @Field({ nullable: true })
  @DeleteDateColumn()
  deleted_at?: Date;

  @Field({ nullable: true })
  @Column({ nullable: true })
  deleted_by: number;
}
