import { ObjectType, Field, Int } from 'type-graphql';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  BaseEntity,
  DeleteDateColumn,
  JoinTable,
  ManyToMany
} from 'typeorm';
import { ActivTypeAttribute } from './ActivTypeAttribute';

@ObjectType()
@Entity()
export class ActivType extends BaseEntity {
  @Field(() => Int)
  @PrimaryGeneratedColumn()
  id!: number;

  @Field(() => String)
  @Column({ unique: true })
  title!: string;

  @Field()
  @Column()
  text!: string;

  @Field()
  @CreateDateColumn()
  created_at: Date;

  @Field()
  @Column()
  created_by: number;

  @Field()
  @UpdateDateColumn()
  updated_at: Date;

  @Field()
  @Column()
  updated_by: number;

  @Field({ nullable: true })
  @DeleteDateColumn()
  deleted_at?: Date;

  @Field({ nullable: true })
  @Column({ nullable: true })
  deleted_by: number;

  @Field(() => [ActivTypeAttribute], { nullable: true })
  @ManyToMany((_) => ActivTypeAttribute, { eager: true })
  @JoinTable({
    name: 'activ_type_attribute_map',
    joinColumn: { name: 'activ_type_id' },
    inverseJoinColumn: { name: 'activ_type_attribute_id' }
  })
  activTypeAttributes: ActivTypeAttribute[];
}
