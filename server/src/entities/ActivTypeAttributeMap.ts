import { ObjectType, Field, Int } from 'type-graphql';
import { Entity, PrimaryColumn, BaseEntity } from 'typeorm';

@ObjectType()
@Entity()
export class ActivTypeAttributeMap extends BaseEntity {
  @Field(() => Int)
  @PrimaryColumn({ unsigned: true })
  activ_type_id!: number;

  @Field(() => Int)
  @PrimaryColumn({ unsigned: true })
  activ_type_attribute_id!: number;
}
