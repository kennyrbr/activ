### Create a migration

`.../activ/server (main*) » npm run migration:create ./src/migrations/ActivTypeTable`

### Run a migration 
Currently I just run when the server runs, but there are scripts in package.json that might do this.

### Tables

```sql
CREATE TABLE `activ_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `text` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL DEFAULT current_timestamp(6),
  `created_by` int(11) NOT NULL,
  `updated_at` datetime(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  `updated_by` int(11) NOT NULL,
  `deleted_at` datetime(6) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_30b1924db7f85e6fe9a79d969d` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
```