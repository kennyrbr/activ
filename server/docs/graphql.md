## graph ql

### ActivType

#### ActivTypes
Example of default parameter value.
```graphql
query ActivTypes($query: String = "") {
  activTypes(query: $query) {
    id
    title
    text
    created_at
    created_by
    updated_at
    updated_by
    deleted_at
    deleted_by
  }
}
```

#### CreateActiveType
```graphql
mutation CreateActiveType($title: String!, $text: String!) {
  createActivType(title: $title, text: $text) {
    type {
      id
      title
      text
      created_at
      updated_at
    }
    errors {
      field
      message
    }
  }
}
```
```json
{
  "title": "chinups",
  "text": "text for pushups"
}
```
### ActivTypeAttribute

#### ActivTypeAttributes
```graphql
query ActivTypeAttributes {
  activTypeAttributes {
    id
    title
    text
    type
    created_at
    created_by
    updated_at
    updated_by
    deleted_at
    deleted_by
  }
}
```

#### CreateActivTypeAttribute
```graphql
mutation CreateActivTypeAttribute($title: String!, $text: String!, $type: String!) {
  createActivTypeAttribute(title: $title, text: $text, type: $type) {
    attribute {
      id
      title
      text
      type
      created_at
      updated_at
    }
    errors {
      field
      message
    }
  }
}
```
```json
{
  "title": "repetitions",
  "text": "text for repetitions",
  "type": "type for repetitions"
}
```



### lirredit

#### from https://studio.apollographql.com/sandbox/explorer for lirredit...

##### create post
```graphql
mutation Mutation($title: String!, $text: String!) {
  createPost(title: $title, text: $text) {
    title
    id
    text
    creatorId
    createdAt
    updatedAt
  }
}
```
```json
{
  "title": "Man Shows Up",
  "text": "text for Man Shows Up"
}
```

##### update post

```graphql
mutation UpdatePost($postId: Int!, $text: String!, $title: String!) {
  updatePost(id: $postId, text: $text, title: $title) {
    id
    title
    text
    creatorId
    createdAt
    updatedAt
  }
}
```
... with variables...

```json
{
  "postId": 8,
  "title": "Woman Shows Up",
  "text": "text for Woman Shows Up"
}
```

##### register
```graphql
mutation Mutation($options: UsernamePasswordInput!) {
  register(options: $options) {
    user {
      id
      username
      email
      createdAt
      updatedAt
    }
    errors {
      field
      message
    }
  }
}
```
```json
{
  "options": {
    "username": "leanne",
    "password": "kenn",
  }
}
```

##### login 

```graphql
query Query($options: UsernamePasswordInput!) {
  login(options: $options) {
    errors {
      field
      message
    }
    user {
      id
      username
      email
      createdAt
      updatedAt
    }
  }
}
```
```json
{
  "options": {
    "username": "leanne",
    "password": "leanne"
   }
}
```

###### me 

```graphql
query Me {
  me {
    id
    username
  }
}
```

##### posts and users

```graphql
query {
  posts {
    id
    title
    text
    creatorId
    createdAt
    updatedAt
  }
  users {
    id
    username
    email
  }
}
```

### 
```graphql
```
```
```json
```