/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  plugins: [require('daisyui')],
  theme: {
    extend: {},
    colors: {
      clean: {
        50: '#FCFCFD',
        100: '#FAF9FA',
        200: '#F2F1F4',
        300: '#EDEBEF',
        400: '#E5E3E8',
        500: '#DFDCE3',
        600: '#B1AABB',
        700: '#857A94',
        800: '#584F63',
        900: '#2D2933'
      },
      fresh: {
        50: '#ECF8F6',
        100: '#DAF1EE',
        200: '#B8E5DE',
        300: '#93D8CD',
        400: '#6DCABC',
        500: '#4ABDAC',
        600: '#38998B',
        700: '#2A7469',
        800: '#1D4F47',
        900: '#0E2522'
      },
      sunshine: {
        50: '#FEF8EB',
        100: '#FDF1D8',
        200: '#FCE1AC',
        300: '#FAD384',
        400: '#F9C55D',
        500: '#F7B733',
        600: '#E79D09',
        700: '#AC7507',
        800: '#714D04',
        900: '#3B2802'
      },
      vermillion: {
        50: '#FFEBE6',
        100: '#FEDBD2',
        200: '#FEB8A4',
        300: '#FD9477',
        400: '#FD714A',
        500: '#FC4A1A',
        600: '#DD3203',
        700: '#A62602',
        800: '#6F1901',
        900: '#370D01'
      }
    },
    plugins: []
  }
};
