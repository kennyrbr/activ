import * as Yup from 'yup';

export const createActivTypeAttributeValidationSchema = Yup.object().shape({
  title: Yup.string().required('Required').min(3, 'Title too short!'),
  text: Yup.string().required('Required').min(3, 'Text too short!'),
  type: Yup.string().required('Required').min(3, 'Type too short!')
});
