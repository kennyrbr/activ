import * as Yup from "yup";

export const createActivTypeValidationSchema = Yup.object().shape({
  title: Yup.string().required("Required").min(3, "Title too short!"),
  text: Yup.string().required("Required").min(3, "Text too short!"),
});
