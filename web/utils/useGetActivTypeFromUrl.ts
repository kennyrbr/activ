import { useActivTypeQuery } from '../generated/graphql';
import { useGetIntId } from './useGetIntId';

export const useGetActivTypeFromUrl = () => {
  const intId = useGetIntId();
  return useActivTypeQuery({
    pause: intId === -1,
    variables: {
      id: intId
    }
  });
};
