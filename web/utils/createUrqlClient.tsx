import { dedupExchange, fetchExchange } from 'urql';
import { cacheExchange, query } from '@urql/exchange-graphcache';
import {
  ActivTypeAttributesDocument,
  ActivTypesDocument,
  ActivTypesQuery,
  CreateActivTypeAttributeMutation,
  CreateActivTypeMutation
} from '../generated/graphql';
import { betterUpdateQuery } from './betterUpdateQuery';

export const createUrqlClient = (ssrExchange: any, _ctx: any) => ({
  // ...add your Client options here
  url: 'http://localhost:4001/graphql',
  exchanges: [
    dedupExchange,
    cacheExchange({
      updates: {
        Mutation: {
          createActivType: (result: CreateActivTypeMutation, _args, cache, _info) => {
            // console.log(`@cacheExchange(createActiveType)`);
            cache.updateQuery({ query: ActivTypesDocument }, (data: any) => {
              // console.log(`@cacheExchange(createActiveType - updateQuery)`);
              // console.log(`result`);
              // console.log(result);
              // console.log(`data`);
              // console.log(data);
              if (result.createActivType.errors) {
                return query;
              }
              data.activTypes.push(result.createActivType.type);
              // console.log(`data`);
              // console.log(data);
              return data;
            });
          },
          createActivTypeAttribute: (result: CreateActivTypeAttributeMutation, _args, cache, _info) => {
            // console.log(`@cacheExchange(createActivTypeAttribute)`);
            cache.updateQuery({ query: ActivTypeAttributesDocument }, (data: any) => {
              // console.log(`@cacheExchange(createActivTypeAttribute - updateQuery)`);
              if (result.createActivTypeAttribute.errors) {
                return query;
              }
              data.activTypes.push(result.createActivTypeAttribute.attribute);
              return data;
            });
          }
        }
      }
    }),
    ssrExchange,
    fetchExchange
  ]
});
