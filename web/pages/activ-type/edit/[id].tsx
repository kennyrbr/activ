import React from 'react';
import { withUrqlClient } from 'next-urql';
import { createUrqlClient } from '../../../utils/createUrqlClient';
import { useGetActivTypeFromUrl } from '../../../utils/useGetActivTypeFromUrl';
import { NavBar } from '../../../components/NavBar';

const ActivType = ({}) => {
  const [{ data, fetching, error }, getActivTypeFromUrl] = useGetActivTypeFromUrl();

  if (fetching) {
    return <div>loading...</div>;
  }

  if (error) {
    return <div>{error.message}</div>;
  }

  if (!data?.activType) {
    return (
      <div>
        <div>could not find activ type</div>
      </div>
    );
  }

  return (
    <>
      <NavBar />
      <div>{data.activType.title}</div>
      <div>{data.activType.text}</div>
      {data?.activType.activTypeAttributes?.map((attribute) => {
        return <div>{attribute.title}</div>;
      })}
    </>
  );
};

export default withUrqlClient(createUrqlClient)(ActivType);
