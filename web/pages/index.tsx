import type { NextPage } from 'next';
import { withUrqlClient } from 'next-urql';
import { useRouter } from 'next/router';
import { ActivTypeAttributes } from '../components/ActivTypeAttributes';
import { ActivTypes } from '../components/ActivTypes';
import { NavBar } from '../components/NavBar';
import { SelectActivType } from '../components/SelectActivType';
import { createUrqlClient } from '../utils/createUrqlClient';

const Index: NextPage = () => {
  return (
    <>
      <NavBar />
      <SelectActivType />
      <ActivTypes />
      <ActivTypeAttributes />
    </>
  );
};

export default withUrqlClient(createUrqlClient)(Index);
