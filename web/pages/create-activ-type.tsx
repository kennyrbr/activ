import { ErrorMessage, Field, Form, Formik, FormikHelpers } from 'formik';
import type { NextPage } from 'next';
import { withUrqlClient } from 'next-urql';
import { useRouter } from 'next/router';
import { NavBar } from '../components/NavBar';
import { useCreateActivTypeMutation } from '../generated/graphql';
import { createUrqlClient } from '../utils/createUrqlClient';
import { toErrorMap } from '../utils/toErrorMap';
import { createActivTypeValidationSchema } from '../validations/createActivTypeValidationSchema';

const styles = {
  label: 'block text-gray-700 text-sm font-bold pt-2 pb-1',
  field:
    'bg-gray-200 text-gray-700 focus:outline-none focus:shadow-outline border border-gray-300 rounded py-2 px-4 block w-full appearance-none',
  button:
    ' bg-gray-700 text-white font-bold py-2 px-4 w-full rounded hover:bg-gray-600',
  errorMsg: 'text-red-500 text-sm'
};

interface CreateActivType {
  title: string;
  text: string;
}

const CreateActiveType: NextPage = () => {
  // const CreateActiveType: NextPage = ({ styles }: any) => {
  const router = useRouter();
  const [, createActivType] = useCreateActivTypeMutation();

  return (
    <>
      <NavBar />
      <Formik
        initialValues={{ title: '', text: '' }}
        validationSchema={createActivTypeValidationSchema}
        onSubmit={async (
          values: CreateActivType,
          { setErrors }: FormikHelpers<CreateActivType>
        ) => {
          // console.log(values);
          const response = await createActivType(values);
          // console.log(response);
          if (response.data?.createActivType.errors) {
            setErrors(toErrorMap(response.data.createActivType.errors));
          } else if (response.data?.createActivType.type) {
            console.log(`@create-activ-type - response: `);
            console.log(response);
            router.push('/');
          }
        }}
      >
        {({ isSubmitting }) => (
          <Form>
            <label className={styles?.label} htmlFor="title">
              Title
            </label>
            <Field
              className={styles?.field}
              name="title"
              placeholder="title"
              label="Title"
            />
            <ErrorMessage
              component="div"
              className={styles?.errorMsg}
              name="title"
            />
            <label className={styles?.label} htmlFor="text">
              Text
            </label>
            <Field
              className={styles?.field}
              name="text"
              placeholder="text"
              label="text"
            />
            <ErrorMessage
              component="div"
              className={styles?.errorMsg}
              name="text"
            />
            <div className="mt-8">
              <button type="submit" disabled={isSubmitting}>
                create
              </button>
            </div>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default withUrqlClient(createUrqlClient)(CreateActiveType);
