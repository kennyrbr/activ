# activ web

`nvm use v16.15.1` - (`node16` alias)

## Run with

`yarn dev`

## Generate graphql types with

`yarn gen`

## installed with ...
- `npx create-next-app --typescript --example with-tailwindcss web`
```
/Volumes/Samsung/a-learning/benawad/fullstackreact/activ (main*) » npx create-next-app --typescript --example with-tailwindcss web
Creating a new Next.js app in /Volumes/Samsung/a-learning/benawad/fullstackreact/activ/web.

Downloading files for example with-tailwindcss. This might take a moment.

Installing packages. This might take a couple of minutes.

yarn install v1.22.18
info No lockfile found.
[1/4] 🔍  Resolving packages...
[2/4] 🚚  Fetching packages...
[3/4] 🔗  Linking dependencies...
[4/4] 🔨  Building fresh packages...
success Saved lockfile.
✨  Done in 15.65s.

Success! Created web at /Volumes/Samsung/a-learning/benawad/fullstackreact/activ/web
Inside that directory, you can run several commands:

  yarn dev
    Starts the development server.

  yarn build
    Builds the app for production.

  yarn start
    Runs the built app in production mode.

We suggest that you begin by typing:

  cd web
  yarn dev

/Volumes/Samsung/a-learning/benawad/fullstackreact/activ (main*) » 
```

- `yarn add urql graphql`
- `yarn add next-urql react-is urql graphql`
- `yarn add @graphql-codegen/typescript-urql`
- `yarn add -D @graphql-codegen/cli @graphql-codegen/typescript @graphql-codegen/typescript-operations`
- copied codegen.yml from lireddit-web
  - could follow https://www.graphql-code-generator.com/docs/getting-started/installation
- `yarn gen`
- 


---
# Next.js + Tailwind CSS Example

This example shows how to use [Tailwind CSS](https://tailwindcss.com/) [(v3.0)](https://tailwindcss.com/blog/tailwindcss-v3) with Next.js. It follows the steps outlined in the official [Tailwind docs](https://tailwindcss.com/docs/guides/nextjs).

## Deploy your own

Deploy the example using [Vercel](https://vercel.com?utm_source=github&utm_medium=readme&utm_campaign=next-example) or preview live with [StackBlitz](https://stackblitz.com/github/vercel/next.js/tree/canary/examples/with-tailwindcss)

[![Deploy with Vercel](https://vercel.com/button)](https://vercel.com/new/git/external?repository-url=https://github.com/vercel/next.js/tree/canary/examples/with-tailwindcss&project-name=with-tailwindcss&repository-name=with-tailwindcss)

## How to use

Execute [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app) with [npm](https://docs.npmjs.com/cli/init), [Yarn](https://yarnpkg.com/lang/en/docs/cli/create/), or [pnpm](https://pnpm.io) to bootstrap the example:

```bash
npx create-next-app --example with-tailwindcss with-tailwindcss-app
# or
yarn create next-app --example with-tailwindcss with-tailwindcss-app
# or
pnpm create next-app --example with-tailwindcss with-tailwindcss-app
```

Deploy it to the cloud with [Vercel](https://vercel.com/new?utm_source=github&utm_medium=readme&utm_campaign=next-example) ([Documentation](https://nextjs.org/docs/deployment)).

---