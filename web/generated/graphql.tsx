import gql from 'graphql-tag';
import * as Urql from 'urql';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  DateTime: any;
};

export type ActivType = {
  __typename?: 'ActivType';
  activTypeAttributes?: Maybe<Array<ActivTypeAttribute>>;
  created_at: Scalars['DateTime'];
  created_by: Scalars['Float'];
  deleted_at?: Maybe<Scalars['DateTime']>;
  deleted_by?: Maybe<Scalars['Float']>;
  id: Scalars['Int'];
  text: Scalars['String'];
  title: Scalars['String'];
  updated_at: Scalars['DateTime'];
  updated_by: Scalars['Float'];
};

export type ActivTypeAttribute = {
  __typename?: 'ActivTypeAttribute';
  created_at: Scalars['DateTime'];
  created_by: Scalars['Float'];
  deleted_at?: Maybe<Scalars['DateTime']>;
  deleted_by?: Maybe<Scalars['Float']>;
  id: Scalars['Int'];
  text: Scalars['String'];
  title: Scalars['String'];
  type: Scalars['String'];
  updated_at: Scalars['DateTime'];
  updated_by: Scalars['Float'];
};

export type ActivTypeAttributeResponse = {
  __typename?: 'ActivTypeAttributeResponse';
  attribute?: Maybe<ActivTypeAttribute>;
  errors?: Maybe<Array<FieldError>>;
};

export type ActivTypeResponse = {
  __typename?: 'ActivTypeResponse';
  errors?: Maybe<Array<FieldError>>;
  type?: Maybe<ActivType>;
};

export type FieldError = {
  __typename?: 'FieldError';
  field: Scalars['String'];
  message: Scalars['String'];
};

export type Mutation = {
  __typename?: 'Mutation';
  createActivType: ActivTypeResponse;
  createActivTypeAttribute: ActivTypeAttributeResponse;
  deleteActivType?: Maybe<Scalars['Boolean']>;
  deleteActivTypeAttribute?: Maybe<Scalars['Boolean']>;
  updateActivType?: Maybe<ActivType>;
  updateActivTypeAttribute?: Maybe<ActivTypeAttribute>;
};


export type MutationCreateActivTypeArgs = {
  text: Scalars['String'];
  title: Scalars['String'];
};


export type MutationCreateActivTypeAttributeArgs = {
  text: Scalars['String'];
  title: Scalars['String'];
  type: Scalars['String'];
};


export type MutationDeleteActivTypeArgs = {
  id: Scalars['Int'];
};


export type MutationDeleteActivTypeAttributeArgs = {
  id: Scalars['Int'];
};


export type MutationUpdateActivTypeArgs = {
  id: Scalars['Int'];
  text: Scalars['String'];
  title: Scalars['String'];
};


export type MutationUpdateActivTypeAttributeArgs = {
  id: Scalars['Int'];
  text: Scalars['String'];
  title: Scalars['String'];
  type: Scalars['String'];
};

export type Query = {
  __typename?: 'Query';
  activType?: Maybe<ActivType>;
  activTypeAttribute?: Maybe<ActivTypeAttribute>;
  activTypeAttributes: Array<ActivTypeAttribute>;
  activTypes: Array<ActivType>;
  hello: Scalars['String'];
};


export type QueryActivTypeArgs = {
  id: Scalars['Int'];
};


export type QueryActivTypeAttributeArgs = {
  id: Scalars['Int'];
};


export type QueryActivTypesArgs = {
  query?: InputMaybe<Scalars['String']>;
};

export type CreateActivTypeMutationVariables = Exact<{
  title: Scalars['String'];
  text: Scalars['String'];
}>;


export type CreateActivTypeMutation = { __typename?: 'Mutation', createActivType: { __typename?: 'ActivTypeResponse', type?: { __typename?: 'ActivType', id: number, title: string, text: string, created_at: any, created_by: number, updated_at: any, updated_by: number } | null, errors?: Array<{ __typename?: 'FieldError', field: string, message: string }> | null } };

export type CreateActivTypeAttributeMutationVariables = Exact<{
  title: Scalars['String'];
  text: Scalars['String'];
  type: Scalars['String'];
}>;


export type CreateActivTypeAttributeMutation = { __typename?: 'Mutation', createActivTypeAttribute: { __typename?: 'ActivTypeAttributeResponse', attribute?: { __typename?: 'ActivTypeAttribute', id: number, title: string, text: string, type: string, created_at: any, created_by: number, updated_at: any, updated_by: number } | null, errors?: Array<{ __typename?: 'FieldError', field: string, message: string }> | null } };

export type ActivTypeQueryVariables = Exact<{
  id: Scalars['Int'];
}>;


export type ActivTypeQuery = { __typename?: 'Query', activType?: { __typename?: 'ActivType', id: number, title: string, text: string, created_at: any, created_by: number, updated_at: any, updated_by: number, deleted_at?: any | null, deleted_by?: number | null, activTypeAttributes?: Array<{ __typename?: 'ActivTypeAttribute', id: number, title: string, type: string }> | null } | null };

export type ActivTypeAttributesQueryVariables = Exact<{ [key: string]: never; }>;


export type ActivTypeAttributesQuery = { __typename?: 'Query', activTypeAttributes: Array<{ __typename?: 'ActivTypeAttribute', id: number, title: string, text: string, type: string, created_at: any, created_by: number, updated_at: any, updated_by: number, deleted_at?: any | null, deleted_by?: number | null }> };

export type ActivTypesQueryVariables = Exact<{
  query?: InputMaybe<Scalars['String']>;
}>;


export type ActivTypesQuery = { __typename?: 'Query', activTypes: Array<{ __typename?: 'ActivType', id: number, title: string, text: string, created_at: any, created_by: number, updated_at: any, updated_by: number, deleted_at?: any | null, deleted_by?: number | null }> };


export const CreateActivTypeDocument = gql`
    mutation CreateActivType($title: String!, $text: String!) {
  createActivType(title: $title, text: $text) {
    type {
      id
      title
      text
      created_at
      created_by
      updated_at
      updated_by
    }
    errors {
      field
      message
    }
  }
}
    `;

export function useCreateActivTypeMutation() {
  return Urql.useMutation<CreateActivTypeMutation, CreateActivTypeMutationVariables>(CreateActivTypeDocument);
};
export const CreateActivTypeAttributeDocument = gql`
    mutation CreateActivTypeAttribute($title: String!, $text: String!, $type: String!) {
  createActivTypeAttribute(title: $title, text: $text, type: $type) {
    attribute {
      id
      title
      text
      type
      created_at
      created_by
      updated_at
      updated_by
    }
    errors {
      field
      message
    }
  }
}
    `;

export function useCreateActivTypeAttributeMutation() {
  return Urql.useMutation<CreateActivTypeAttributeMutation, CreateActivTypeAttributeMutationVariables>(CreateActivTypeAttributeDocument);
};
export const ActivTypeDocument = gql`
    query ActivType($id: Int!) {
  activType(id: $id) {
    id
    title
    text
    created_at
    created_by
    updated_at
    updated_by
    deleted_at
    deleted_by
    activTypeAttributes {
      id
      title
      type
    }
  }
}
    `;

export function useActivTypeQuery(options: Omit<Urql.UseQueryArgs<ActivTypeQueryVariables>, 'query'>) {
  return Urql.useQuery<ActivTypeQuery>({ query: ActivTypeDocument, ...options });
};
export const ActivTypeAttributesDocument = gql`
    query ActivTypeAttributes {
  activTypeAttributes {
    id
    title
    text
    type
    created_at
    created_by
    updated_at
    updated_by
    deleted_at
    deleted_by
  }
}
    `;

export function useActivTypeAttributesQuery(options?: Omit<Urql.UseQueryArgs<ActivTypeAttributesQueryVariables>, 'query'>) {
  return Urql.useQuery<ActivTypeAttributesQuery>({ query: ActivTypeAttributesDocument, ...options });
};
export const ActivTypesDocument = gql`
    query ActivTypes($query: String) {
  activTypes(query: $query) {
    id
    title
    text
    created_at
    created_by
    updated_at
    updated_by
    deleted_at
    deleted_by
  }
}
    `;

export function useActivTypesQuery(options?: Omit<Urql.UseQueryArgs<ActivTypesQueryVariables>, 'query'>) {
  return Urql.useQuery<ActivTypesQuery>({ query: ActivTypesDocument, ...options });
};