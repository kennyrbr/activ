import { useRouter } from 'next/router';
import { useActivTypeAttributesQuery } from '../generated/graphql';
import { CREATE_ACTIV_TYPE_ATTRIBUTE_ROUTE } from '../utils/constants';

interface ActivTypeAttributesProps {}

export const ActivTypeAttributes: React.FC<ActivTypeAttributesProps> = ({}) => {
  const router = useRouter();
  const [{ data, fetching }] = useActivTypeAttributesQuery();

  let body = <div>loading...</div>;
  if (!fetching) {
    body = (
      <>
        {data!.activTypeAttributes.map((type) => (
          <div
            className="px-4 py-8 rounded-tr-xl bg-fresh-500 hover:bg-fresh-300 font-bold text-clean-300"
            key={type.id}
          >
            {type.title}
          </div>
        ))}

        <div
          className="px-4 py-8 rounded-tr-xl bg-sunshine-500 hover:bg-sunshine-300 font-bold text-clean-300"
          onClick={() => router.push(CREATE_ACTIV_TYPE_ATTRIBUTE_ROUTE)}
        >
          create activ type attribute
        </div>
      </>
    );
  }
  return <div className="m-4 grid grid-cols-3 gap-4">{body}</div>;
};
