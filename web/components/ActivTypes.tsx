import { useRouter } from 'next/router';
import { useActivTypesQuery } from '../generated/graphql';
import { ACTIV_TYPE_ROUTE, CREATE_ACTIV_TYPE_ROUTE } from '../utils/constants';

interface ActivTypesProps {}

export const ActivTypes: React.FC<ActivTypesProps> = ({}) => {
  const router = useRouter();
  const [{ data, fetching }] = useActivTypesQuery();

  let body = <div>loading...</div>;
  if (!fetching) {
    body = (
      <>
        {data!.activTypes.map((type) => (
          <div
            className="px-4 py-8 rounded-tr-xl bg-fresh-500 hover:bg-fresh-300 font-bold text-clean-300"
            key={type.id}
            onClick={() => router.push(`${ACTIV_TYPE_ROUTE}/${type.id}`)}
          >
            {type.title}
          </div>
        ))}

        <div
          className="px-4 py-8 rounded-tr-xl bg-sunshine-500 hover:bg-sunshine-300 font-bold text-clean-300"
          onClick={() => router.push(CREATE_ACTIV_TYPE_ROUTE)}
        >
          create activ type
        </div>
      </>
    );
  }
  return <div className="m-4 grid grid-cols-3 gap-4">{body}</div>;
};
