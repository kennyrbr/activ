import { useRouter } from 'next/router';
import { MutableRefObject, useEffect, useRef, useState } from 'react';
import { useActivTypesQuery } from '../generated/graphql';
import { CREATE_ACTIV_ROUTE } from '../utils/constants';

interface SelectActivTypeProps {}

// const handleActivTypeCreateClick = (e) => {
//   console.log(`@handleActivTypeCreateClick`);
//   Router.push({pathname: '', query: {activ_type_id: e.target.id }}
// };

const useFocus = (): [any, (_: boolean) => void] => {
  const htmlElRef: MutableRefObject<any> = useRef(null);
  const setFocus = (focus: boolean): void => {
    focus ? htmlElRef?.current?.focus?.() : htmlElRef?.current?.blur?.();
  };

  return [htmlElRef, setFocus];
};

export const SelectActivType: React.FC<SelectActivTypeProps> = ({}) => {
  const router = useRouter();
  const [searchData, setSearchData] = useState<string>('');
  const [{ data, fetching }] = useActivTypesQuery({ variables: { query: searchData } });
  const [elRef, setElFocus] = useFocus();

  let body = <div>loading...</div>;
  if (!fetching) {
    body = (
      <>
        {data!.activTypes.map((type) => (
          <div
            className="px-2 py-1 rounded-tr-xl bg-fresh-500 hover:bg-fresh-300 font-bold text-clean-300"
            key={type.id}
            id={`${type.id}`}
            onClick={(e) => {
              console.log(`@handleActivTypeCreateClick`);
              console.log(e);
              const activ_type_id = (e.target as any).id;
              router.push({ pathname: CREATE_ACTIV_ROUTE, query: { activ_type_id } });
            }}
          >
            {type.title}
          </div>
        ))}
      </>
    );
  }

  // https://javascript.plainenglish.io/how-to-detect-a-sequence-of-keystrokes-in-javascript-83ec6ffd8e93
  const handleKeyDown = (event: any) => {
    console.log(`@handleKeyDown()`);
    console.log(event);

    const charList = 'abcdefghijklmnopqrstuvwxyz0123456789';
    const key = event.key.toLowerCase();

    // we are only interested in alphanumeric keys
    if (charList.indexOf(key) === -1) return;

    // TODO: add in backspace? esc? enter?
    setSearchData((prevSearchData) => prevSearchData + key);
  };

  return (
    <div className="m-4 grid grid-cols-3 gap-4">
      <div
        className="relative px-4 py-8 rounded-tr-xl bg-sunshine-500 hover:bg-sunshine-300 font-bold text-clean-300 max-h-[5.5rem]"
        tabIndex={-1}
        onKeyDown={(e) => handleKeyDown(e)}
        onMouseEnter={(e) => setElFocus(true)}
        onMouseLeave={(e) => setElFocus(false)}
        ref={elRef}
      >
        create activ
        <div className="absolute top-1 right-1 overflow-y-scroll max-h-20">
          <div>{body}</div>
        </div>
      </div>
      <div>{searchData}</div>
    </div>
  );
};
