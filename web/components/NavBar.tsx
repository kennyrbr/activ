import { useRouter } from 'next/router';
import { HOME_ROUTE } from '../utils/constants';

interface NavBarProps {}

export const NavBar: React.FC<NavBarProps> = ({}) => {
  const router = useRouter();
  return (
    <nav className="pb-1 bg-fresh-100 border-b-2 border-b-vermillion-300">
      <span
        className="m-3 text-6xl text-vermillion-500"
        onClick={() => {
          router.push(HOME_ROUTE);
        }}
      >
        activ
      </span>
    </nav>
  );
};
